package com.example.mydictionary;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.example.mydictionary.Repository.ArticleRepository;
import com.example.mydictionary.database.Article;
import com.example.mydictionary.database.InfoDao;
import com.example.mydictionary.database.MyDatabaseHelper;

import com.example.mydictionary.localquery.JsonHelper;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Query_fragement extends Fragment {
    private String TAG = "MainActivity";
    private SQLiteDatabase db;
    MyDatabaseHelper helper;
    public String cword = null;
    public String token = null;
    EditText editText;
    TextView textView;
    ImageButton cbt ;
    Onclick onclick=new Onclick();
    LinearLayout linearLayouts;
    List<LinearLayout> linearLayoutList = new ArrayList<LinearLayout>();
    Article[] articles;
    private ConnectivityManager connectivityManager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.query_layout,null);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        return view;
    }

    Runnable ArticleTask = new Runnable(){
        @Override
        public void run(){

            articles = ArticleRepository.getArticlesFromInternet();
        }
    };
    private Handler handler = new Handler(){
        //4.handler获取消息进行处理
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            //1.获取数据
            //Basic basic = word.getBasic();
            String explains = null;
            //explains = ((Basic) basic).getStrings(basic.getExplains());
            explains=(String)msg.obj;
            //2.在界面显示
            textView.setText(explains);
//            if(TextUtils.isEmpty(textView.getText().toString())){
//                cbt.setVisibility(View.INVISIBLE);
//            }else{
//                cbt.setVisibility(View.VISIBLE);
//            }
        }
    };

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //
        helper = new MyDatabaseHelper(getActivity());
        cbt = getActivity().findViewById(R.id.collectbt);
        if(getArguments() !=null){
            Bundle bundle =getArguments();
            cword = bundle.getString("cword");
            token = bundle.getString("token");
            //Log.d(TAG, "Bundle c: " + cword);
            Log.d(TAG, "Bundle t: " + token);
        }
        if(cword !=null){
            editText = getActivity().findViewById(R.id.et);
            textView = getActivity().findViewById(R.id.tv);
            ImageButton qbt = getActivity().findViewById(R.id.querybt);
            qbt.setOnClickListener(onclick);
            editText.setText(cword);
            queryWord(cword);
            qbt.setOnClickListener(onclick);
        }else{
            editText = getActivity().findViewById(R.id.et);
            textView = getActivity().findViewById(R.id.tv);
            ImageButton qbt = getActivity().findViewById(R.id.querybt);
            qbt.setOnClickListener(onclick);
        }
        //
        if(isConnectNet(getActivity())){
            new Thread(ArticleTask).start();

            linearLayouts =(LinearLayout)getActivity().findViewById(R.id.ll_for_article);

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            for(int i=0;i<articles.length;i++){

                LinearLayout ll = new LinearLayout(getActivity());
                ll.setId(i);
                LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 240);
                layoutParams.setMargins(54, 0, 84, 0);
                ll.setLayoutParams(layoutParams);
                ll.setOrientation(LinearLayout.HORIZONTAL);

                TextView tv = new TextView(getActivity());
                LinearLayout.LayoutParams tvParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1);
                tvParams.gravity = Gravity.RIGHT;
                tv.setLayoutParams(tvParams);
                tv.setTextSize(14);
                tv.setText(articles[i].getTitle());

                ImageView imageView = new ImageView(getActivity());
                LinearLayout.LayoutParams ivParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1);
                ivParams.gravity=Gravity.LEFT;
                imageView.setLayoutParams(ivParams);
                Glide.with(getActivity()).load(articles[i].getImageUrl()).into(imageView);


                ll.addView(imageView);
                ll.addView(tv);
                String url = articles[i].getUrl();

                int finalI = i;
                ll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //监听按钮，如果点击，就跳转
                        Intent intent = new Intent();
                        intent.putExtra("url",articles[finalI].getUrl());
                        System.out.println(articles[finalI].getUrl());
                        //前一个（MainActivity.this）是目前页面，后面一个是要跳转的下一个页面
                        intent.setClass(getActivity(),webview_activity.class);
                        startActivity(intent);
                    }
                });


                linearLayouts.addView(ll);
                linearLayoutList.add(ll);

            }
        }

        cbt.setOnClickListener(onclick);

    }



    public void queryWord(String s){
        String url = "http://fanyi.youdao.com/openapi.do?keyfrom=pythonxiaodeng&key=929705525&type=data&doctype=json&version=1.1&q="+s;
        OkHttpClient okHttpClient = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(url)
                .get()
                .build();

        //2.创建Call对象
        Call call = okHttpClient.newCall(request);
        //3.重写Call对象的enqueue方法
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d(TAG, "onFailure: ");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                //1.获取响应数据
                String str = response.body().string();
                //Log.d(TAG, "onResponse: " + str);
                try {
                    //2.封装成json对象
                    JSONObject jsonObject = new JSONObject(str);
                    //3.转为java对象
                    //Log.d(TAG, "onResponse2: " + str);
                    if(jsonObject.has("basic")){
                        //4.创建message,包裹信息
                        JSONObject json = jsonObject.getJSONObject("basic");
                        JSONArray jsonArray=json.getJSONArray("explains");
                        String mess="";
                        for(int i=0;i<jsonArray.length();i++){
                            mess+=(String)jsonArray.get(i)+"\n";
                        }
                        Message message = new Message();
                        message.obj = mess;
                        //5.发送message给handler
                        handler.sendMessage(message);
                    }else if(jsonObject.has("translation")){
                        JSONArray jsonArray=jsonObject.getJSONArray("translation");
                        String mess="";
                        for(int i=0;i<jsonArray.length();i++){
                            mess+=(String)jsonArray.get(i)+"\n";
                        }
                        //4.创建message,包裹信息
                        Message message = new Message();
                        message.obj = mess;
                        //5.发送message给handler
                        handler.sendMessage(message);
                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }
        });




    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    public static Query_fragement Instance(String s){
        Query_fragement qf = new Query_fragement();
        Bundle bundle =new Bundle();
        bundle.putString("word",s);
        qf.setArguments(bundle);
        return qf;
    }
    public class Onclick implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            String word;
            switch (v.getId()){
                case R.id.collectbt:
                    editText = getActivity().findViewById(R.id.et);
                    ///db = helper.getWritableDatabase();
                    word = editText.getText().toString();
                    //Toast.makeText(getActivity(),word,Toast.LENGTH_SHORT).show();
                    if(token.equals("unlogin")){
                        Toast.makeText(getActivity(),"请登录",Toast.LENGTH_SHORT).show();
                    }else{
                       if(TextUtils.isEmpty(word)){
                           Toast.makeText(getActivity(),"收藏单词为空",Toast.LENGTH_SHORT).show();
                       }else {
                           InfoDao infoDao =new InfoDao(getActivity());
                           Boolean b=infoDao.ninsert(word,token);
                           Log.d(TAG, String.valueOf(b));
                           if(!b){
                               Toast.makeText(getActivity(),"此单词已收藏",Toast.LENGTH_SHORT).show();
                           }
                       }
                    }
                case R.id.querybt:
                    editText = getActivity().findViewById(R.id.et);
                    textView = getActivity().findViewById(R.id.tv);
                    word = editText.getText().toString();
                    if(word==null||word.equals("")){
                        Toast.makeText(getContext(),"查询单词为空",Toast.LENGTH_SHORT).show();
                        textView.setText(null);
                    }
                    else {
                        if(isConnectNet(getActivity())) {
                            queryWord(word);
                        }else {
                            JsonHelper jsonHelper = new JsonHelper();
                            String Dictstr = jsonHelper.getJson(getActivity(),"dict.json");
                            try {
                                JSONArray DictArry =new JSONArray(Dictstr);
                                String localquery=jsonHelper.Query(DictArry,word);
                                if(localquery==null){
                                    Toast.makeText(getContext(),"本地词库无此单词",Toast.LENGTH_SHORT).show();

                                }else {
                                    textView.setText(localquery);
                                    Log.d(TAG, "翻译为：" + localquery);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            }
        }
        }

    private boolean isConnectNet(Context context){
        connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);//获取当前网络的连接服务
        NetworkInfo info = connectivityManager.getActiveNetworkInfo(); //获取活动的网络连接信息
        if (info != null && info.isAvailable() && info.isConnected()) {
            Log.d(TAG,"Connected");

            //当前没有已激活的网络连接（表示用户关闭了数据流量服务，也没有开启WiFi等别的数据服务）
            return true;
        } else {
            return false;

        }
    }
    public static Query_fragement Instance2(String s,String token){
        Query_fragement qf = new Query_fragement();
        Bundle bundle =new Bundle();
        bundle.putString("token",token);
        bundle.putString("cword",s);
        qf.setArguments(bundle);
        return qf;
    }
    public static Query_fragement Instance3(String token){
        Query_fragement qf = new Query_fragement();
        Bundle bundle =new Bundle();
        bundle.putString("token",token);
        qf.setArguments(bundle);
        return qf;
    }
}
