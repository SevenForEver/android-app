package com.example.mydictionary.database;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentManager;

import com.example.mydictionary.Query_fragement;
import com.example.mydictionary.R;

import java.util.ArrayList;

public class QueryAdapter extends BaseAdapter{
    private Context mContext;
    private ArrayList<InfoBean> arrayList;
    private Query_fragement qf;
    private String token;

    public QueryAdapter(Context mContext, ArrayList<InfoBean> arrayList,String token) {
        this.mContext = mContext;
        this.arrayList = arrayList;
        this.token=token;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //复用convertView
        View view = null;
        if(convertView != null){
            view = convertView;
        }else {
            view =  View.inflate(mContext, R.layout.item_database_layout, null);
        }

        //找到控件
        TextView word = (TextView) view.findViewById(R.id.word_text);



        //找到内容
        InfoBean infoBean = arrayList.get(position);

        //设置内容
        word.setText(infoBean.word);
        String dword=word.getText().toString();
        Button deleteC = view.findViewById(R.id.CollectionDeleteBt);
        deleteC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(mContext,dword,Toast.LENGTH_SHORT).show();

                InfoDao infodao =new InfoDao(mContext);
                Boolean b=infodao.delete(dword,token);
                if(b){
                    Toast.makeText(mContext,"success",Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }



}
