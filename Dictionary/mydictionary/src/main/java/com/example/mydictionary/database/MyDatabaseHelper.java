package com.example.mydictionary.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MyDatabaseHelper extends SQLiteOpenHelper {
    private static SQLiteOpenHelper SQLInstance;
    public static final String TABLE_NAME = "dictionary";
    final String CREATE_TABLE_SQL="create table dictionary (_id integer primary key autoincrement,word text)";
//    public static synchronized SQLiteOpenHelper getSQLInstance(Context context){
//        if(SQLInstance==null){
//            SQLInstance = new MysqlOpenHelper(context,"test.db",null,1);
//        }
//        return SQLInstance;
//    }
    public MyDatabaseHelper(Context context) {
        super(context, "test.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_SQL);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i("词典","--update"+oldVersion+"-->"+ newVersion);

    }
}
