package com.example.mydictionary.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mydictionary.MainActivity;
import com.example.mydictionary.Net;
import com.example.mydictionary.R;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {
    Button login ;
    EditText Lusername ;
    EditText Lpassword ;
    Net net;
    String status;
    String message;
    String token;
    Button regist;
    Button jump;
    private String TAG = "MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Onclick onclick=new Onclick();
        login = findViewById(R.id.login);
        regist=findViewById(R.id.registnow);
        jump=findViewById(R.id.jump);
        jump.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                intent.putExtra("username","unlogin");
                startActivity(intent);
            }
        });
        regist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, Regist.class);
                //intent.putExtra("username","unlogin");
                startActivity(intent);
            }
        });
        login.setOnClickListener(onclick);
    }
    public class Onclick implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.login:
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    Lusername =findViewById(R.id.username);
                    Lpassword=findViewById(R.id.password);
                    String username = Lusername.getText().toString();
                    String password = Lpassword.getText().toString();
                    new Thread(){
                        @Override
                        public void run()
                        {
                            Looper.prepare();
                            net=new Net();
                            JSONObject user = net.login(username,password);//把网络访问的代码放在这里
                            try {
                                if(user.has("token")){
                                    token=user.getString("token");
                                    //Log.d(TAG,token);
                                    intent.putExtra("username",token);
                                    startActivity(intent);
                                }else {
                                    message=user.getString("message");
                                    Toast.makeText(getApplication(),message,Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {

                                e.printStackTrace();
                            }
                            Looper.loop();
                        }
                    }.start();
            }
        }
    }
}