package com.example.mydictionary.localquery;

import android.content.Context;
import android.content.res.AssetManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class JsonHelper {
    public String getJson(Context context, String fileName){

        StringBuilder stringBuilder = new StringBuilder();
        try {
            AssetManager assetManager = context.getAssets();
            BufferedReader bf = new BufferedReader(new InputStreamReader(
                    assetManager.open(fileName)));
            String line;
            while ((line = bf.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        //JSONArray DicObject = new JSONArray(stringBuilder.toString());
        return stringBuilder.toString();
    }
    public String Query(JSONArray jsonArray,String qword) throws JSONException {
        String result = null;
        for (int i = 0; i < jsonArray.length(); i++) {
            // JSON数组里面的具体-JSON对象
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String word = jsonObject.getString("word");
            if(word.equals(qword)){
                result=jsonObject.getString("explanation");
                return result;
            }

        }
        return result;
    }

}
