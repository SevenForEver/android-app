package com.example.mydictionary.login;

import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mydictionary.Net;
import com.example.mydictionary.R;

import org.json.JSONException;
import org.json.JSONObject;

public class Regist extends AppCompatActivity {
   EditText username;
   EditText password;
   Button regist;
    Net net;
    String status;
    String message;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regist);
        username=findViewById(R.id.regist_username);
        password=findViewById(R.id.regist_password);
        regist=findViewById(R.id.regist);
        regist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = username.getText().toString();
                String Pword = password.getText().toString();
                new Thread(){
                    @Override
                    public void run()
                    {
                        Looper.prepare();
                        net=new Net();
                        JSONObject user = net.register(name,Pword);//把网络访问的代码放在这里
                        try {
                            status=user.getString("status");
                            if(status.equals("success")){
                                Toast.makeText(getApplication(),"注册成功",Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(Regist.this, LoginActivity.class);
                                startActivity(intent);
                            }else {
                                message=user.getString("message");
                                Toast.makeText(getApplication(),message,Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                        Looper.loop();
                    }
                }.start();
            }
        });
    }
}