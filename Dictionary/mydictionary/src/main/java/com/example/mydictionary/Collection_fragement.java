package com.example.mydictionary;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.mydictionary.database.InfoBean;
import com.example.mydictionary.database.InfoDao;
import com.example.mydictionary.database.MyDatabaseHelper;
import com.example.mydictionary.database.QueryAdapter;
import com.example.mydictionary.login.LoginActivity;

import java.util.ArrayList;

public class Collection_fragement extends Fragment {

    private MyDatabaseHelper DBtest;
    private Query_fragement query_fragement;
    private String TAG = "MainActivity";
    private Query_fragement qf;
    private String username;
    ListView listview_1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.collection_layout,container,false);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(getArguments()!=null){
            Bundle bundle =getArguments();
            username = bundle.getString("username");
            Log.d(TAG, "username is: " + username);
        }
        //DBtest = new MyDatabaseHelper(getActivity());
        //InfoDao infodao =new InfoDao(getActivity());
        listview_1 = getActivity().findViewById(R.id.Listv_1);
        Onclick onclick=new Onclick();
        Button query_collection =getActivity().findViewById(R.id.query_word);
        query_collection.setOnClickListener(onclick);
        Button exit =getActivity().findViewById(R.id.exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
            }
        });
        Button close =getActivity().findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startMain = new Intent(Intent.ACTION_MAIN);
                startMain.addCategory(Intent.CATEGORY_HOME);
                startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                getActivity().startActivity(startMain);
                System.exit(0);//推出程序
            }
        });
        listview_1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                InfoDao infodao =new InfoDao(getActivity());
                TextView textView=view.findViewById(R.id.word_text);
                String s = textView.getText().toString();
//              Toast.makeText(getContext(),s,Toast.LENGTH_SHORT).show();
                qf=new Query_fragement().Instance2(s,username);
                //qf=new Query_fragement().Instance(s);
                getFragmentManager().beginTransaction().replace(R.id.fragment,qf).commitAllowingStateLoss();

            }
        });

    }
    public static Collection_fragement Instance(String s){
        Collection_fragement cf = new Collection_fragement();
        Bundle bundle =new Bundle();
        bundle.putString("username",s);
        cf.setArguments(bundle);
        return cf;
    }
    public class Onclick implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.query_word:
                    new Thread() {
                        @Override
                        public void run() {
                            listview_1 = getActivity().findViewById(R.id.Listv_1);
                            InfoDao infodao =new InfoDao(getActivity());
                            listview_1.post(new Runnable() {
                                @Override
                                public void run() {
                                    if(username.equals("unlogin")){
                                        Toast.makeText(getActivity(),"未登录",Toast.LENGTH_SHORT).show();
                                    }else{
                                        Log.d(TAG,"查询的账号为："+ username);
                                        ArrayList<InfoBean> arrayList = infodao.netqueryall(username);
                                        QueryAdapter queryAdapter = new QueryAdapter(getActivity(),arrayList,username);
                                        listview_1.setAdapter(queryAdapter);//把网络访问的代码放在这里
                                    }
                                }
                            });
                        }
                    }.start();
            }
        }
    }
}
