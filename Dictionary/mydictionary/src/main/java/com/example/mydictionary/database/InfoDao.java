package com.example.mydictionary.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.mydictionary.Net;

import java.util.ArrayList;
import java.util.List;

public class InfoDao {
    private MyDatabaseHelper mySqliteOpenHelper;
    private String TAG = "MainActivity";
    public InfoDao(Context context){
        //创建一个帮助类对象
        mySqliteOpenHelper = new MyDatabaseHelper(context);
    }
    public ArrayList<InfoBean> queryall(){

        ArrayList<InfoBean> list = new ArrayList<InfoBean>();
        SQLiteDatabase db = mySqliteOpenHelper.getReadableDatabase();
        Cursor cursor = db.query("dictionary", new String[]{"_id","word"},null,null, null, null, "_id asc");

        if(cursor != null && cursor.getCount() >0){

            while(cursor.moveToNext()){
                InfoBean bean = new InfoBean();
                //bean. id = cursor.getInt(0)+"";
                bean. word = cursor.getString(1);

                list.add(bean);


            }
            cursor.close();
        }
        db.close();
        return list;
    }
    public ArrayList<InfoBean> netqueryall(String s){
        ArrayList<InfoBean> list = new ArrayList<InfoBean>();
        Net net =new Net();
        List<String> word=net.read(s);
        for (int i = 0; i < word.size(); i++){
            InfoBean bean = new InfoBean();
            bean.word = word.get(i);
            Log.d(TAG, bean.word);
            list.add(bean);
        };
        return list;
    }
    public Boolean delete(String word,String token){
        Net net =new Net();
        Log.d(TAG, word);
        Boolean b = net.delete(token,word);
        return b;
    }


    public Boolean ninsert(String word,String token){
        Net net =new Net();
        Log.d(TAG, "you will insert"+word);
        Boolean b = net.save(token,word);
        return b;
    }
}
