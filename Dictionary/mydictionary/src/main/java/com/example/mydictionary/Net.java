package com.example.mydictionary;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
public class Net {
    private String defaultUrl = "http://39.96.22.45:5000/dictionary/api/";

    public JSONObject register(String username,String password){
        JSONObject result= new JSONObject();
        try {
            //创建连接
            URL url = new URL(defaultUrl+"user/register"+"?username="+URLEncoder.encode(username, "utf-8")+"&password="+password);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            //connection.setRequestProperty("token",value);
            connection.setRequestMethod("GET");
            connection.setUseCaches(false);
            connection.setInstanceFollowRedirects(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.connect();

            DataOutputStream out = new
                    DataOutputStream(connection.getOutputStream());
            out.flush();
            out.close();

            BufferedReader reader = null;
            if (connection.getResponseCode() == 200) {
                reader = new BufferedReader(
                        new InputStreamReader(connection.getInputStream())
                );
                String data = "";
                StringBuilder builder = new StringBuilder();
                while ((data = reader.readLine()) != null) {
                    builder.append(data);
                }
                result= new JSONObject(String.valueOf(builder));
            }
            connection.disconnect();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }
    public JSONObject login(String username,String password){
        JSONObject result= new JSONObject();
        try {
            //创建连接
            URL url = new URL(defaultUrl+"user/login"+"?username="+URLEncoder.encode(username, "utf-8")+"&password="+password);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            //connection.setRequestProperty("token",value);
            connection.setRequestMethod("POST");
            connection.setUseCaches(false);
            connection.setInstanceFollowRedirects(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.connect();
            // POST请求
            DataOutputStream out = new
                    DataOutputStream(connection.getOutputStream());
            JSONObject obj = new JSONObject();
            obj.put("username", username);
            obj.put("password", password);
            String json = URLEncoder.encode(obj.toString(), "utf-8");
            out.writeBytes(json);
            out.flush();
            out.close();
            // 读取响应
            BufferedReader reader = null;
            if (connection.getResponseCode() == 200) {
                reader = new BufferedReader(
                        new InputStreamReader(connection.getInputStream())
                );
                String data = "";
                StringBuilder builder = new StringBuilder();
                while ((data = reader.readLine()) != null) {
                    builder.append(data);
                }
                result= new JSONObject(String.valueOf(builder));
            }
            connection.disconnect();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Boolean save(String token,String word){
        JSONObject result= new JSONObject();
        try {
            //创建连接
            URL url = new URL(defaultUrl+"saver"+"?word="+URLEncoder.encode(word, "utf-8"));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestProperty("token",token);
            connection.setRequestMethod("POST");
            connection.setUseCaches(false);
            connection.setInstanceFollowRedirects(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.connect();
            // POST请求
            DataOutputStream out = new
                    DataOutputStream(connection.getOutputStream());
            JSONObject obj = new JSONObject();
            obj.put("word", word);
            String json = URLEncoder.encode(obj.toString());
            out.writeBytes(json);
            out.flush();
            out.close();
            // 读取响应
            BufferedReader reader = null;
            if (connection.getResponseCode() == 200) {
                reader = new BufferedReader(
                        new InputStreamReader(connection.getInputStream())
                );
                String data = "";
                StringBuilder builder = new StringBuilder();
                while ((data = reader.readLine()) != null) {
                    builder.append(data);
                }
                result= new JSONObject(String.valueOf(builder));
            }
            connection.disconnect();
            if (result.getString("status").equals("success")){
                return true;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }
    public List<String> read(String token){
        List<String> res = new ArrayList<String>();
        JSONArray result= new JSONArray();
        try {
            //创建连接
            URL url = new URL(defaultUrl+"saver");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestProperty("token",token);
            connection.setUseCaches(false);
            connection.setInstanceFollowRedirects(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.connect();
            // POST请求

            // 读取响应
            BufferedReader reader = null;
            if (connection.getResponseCode() == 200) {
                reader = new BufferedReader(
                        new InputStreamReader(connection.getInputStream())
                );
                String data = "";
                StringBuilder builder = new StringBuilder();
                while ((data = reader.readLine()) != null) {
                    builder.append(data);
                }
                result= new JSONArray(String.valueOf(builder));
            }
            connection.disconnect();
            for(int p=0;p<result.length();p++){
                res.add(result.getString(p));
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }catch (JSONException e) {
            e.printStackTrace();
        }
        return res;
    }
    public Boolean delete(String token,String word){
        List<String> res = new ArrayList<String>();
        JSONObject result= new JSONObject();
        try {
            //创建连接
            URL url = new URL(defaultUrl+"delete"+"?word="+URLEncoder.encode(word, "utf-8"));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestProperty("token",token);
            connection.setUseCaches(false);
            connection.setInstanceFollowRedirects(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.connect();
            // POST请求

            // 读取响应
            BufferedReader reader = null;
            if (connection.getResponseCode() == 200) {
                reader = new BufferedReader(
                        new InputStreamReader(connection.getInputStream())
                );
                String data = "";
                StringBuilder builder = new StringBuilder();
                while ((data = reader.readLine()) != null) {
                    builder.append(data);
                }
                result= new JSONObject(String.valueOf(builder));
            }
            connection.disconnect();
            if (result.get("status").equals("success")){
                return  true;
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }
}
