package com.example.mydictionary;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.google.android.material.bottomnavigation.BottomNavigationView;


public class MainActivity extends AppCompatActivity {
    private Query_fragement qf;
    private String username=null;
    private String TAG = "MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.activity_main);
        Intent intent =getIntent();
        username=intent.getStringExtra("username");
        Log.d(TAG,"userStatus="+username);
        BottomNavigationView b = findViewById(R.id.change);
        b.setOnNavigationItemSelectedListener(N);
        qf=new Query_fragement().Instance3(username);
        getSupportFragmentManager().beginTransaction().add(R.id.fragment,qf).commitAllowingStateLoss();
    }
    BottomNavigationView.OnNavigationItemSelectedListener N = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft =fm.beginTransaction();
            Fragment f =null;
            switch (item.getItemId()){
                case R.id.Query:
                    if(username.isEmpty()){
                        f=new Query_fragement();
                    }else {
                        f=new Query_fragement().Instance3(username);
                    }
                    ft.replace(R.id.fragment,f);
                    ft.commitAllowingStateLoss();
                    return true;
                case R.id.Weblink:
                    f= new Weblink_fragement();
                    ft.replace(R.id.fragment,f);
                    ft.commitAllowingStateLoss();
                    return true;
                case R.id.Collection:
                    if(username.isEmpty()){
                        f=new Collection_fragement();
                    }else {
                        f=new Collection_fragement().Instance(username);
                    }
                    ft.replace(R.id.fragment,f);
                    ft.commitAllowingStateLoss();
                    return true;
                default:
                    return false;
            }
        }
    };

}