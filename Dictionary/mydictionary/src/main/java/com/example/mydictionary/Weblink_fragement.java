package com.example.mydictionary;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class Weblink_fragement extends Fragment {
    private  static SearchView searchView;
    private  static WebView webView;
    private String TAG = "MainActivity";
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.weblink_layout,null);
        return view;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        searchView.setSubmitButtonEnabled(true);/// 设置该SearchView显示确认搜索按钮
        webView.getSettings().setJavaScriptEnabled(true);//如果页面中使用了JavaScript，不加代码页面不显示
        webView.setWebViewClient(new WebViewClient(){//如果不加此方法将会在浏览器中打开而不是运行的项目中重点内容
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                String strURI = (query);
                strURI = strURI.trim();
                //如果查询内容为空提示
                if (query.isEmpty()){
                    Toast.makeText(getActivity(), "查询内容不能为空!", Toast.LENGTH_SHORT)
                            .show();
                }
                //否则则以参数的形式从http://dict.youdao.com/m取得数据，加载到WebView里.
                else
                {
                    String strURL1 = "https://dict.youdao.com/m/search?keyfrom=dict.mindex&q="
                            + strURI;
                    webView.loadUrl(strURL1);
//                    webView.loadUrl(strURL2);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });


    }
    public void init(){
        searchView =getActivity().findViewById(R.id.sv);
        webView =getActivity().findViewById(R.id.wv);
//        webView1=getActivity().findViewById(R.id.ciba);

    }

}
